include("alltypes.jl")

# function to check element in node
function checkElementInNode(treeNodes, id, elem)

    # initialization
    indExist = false
    # go through all elements of tree node
    for i in 1:size(treeNodes[id].graphNodes, 1)
        # check equality of two elements
        if treeNodes[id].graphNodes[i] == elem
            # change value of boolean indicator
            indExist = true
            # exit loop
            break
        end
    end
    # return boolean indicator
    return indExist

end

# function to check element in array
function checkElementInArray(array, elem)

    # default answer
    if size(array, 1) == 0
        return false
    end
    # check element in array
    indElem = false
    for i in 1:size(array, 1)
        if array[i] == elem
            indElem = true
            break
        end
    end
    # return boolean indicator
    return indElem

end

# function to check subset condition for specified node
function checkSubsetCondition(treeNodes, i)

    # initialization
    indSubset = false
    # go through all tree nodes
    for j in 1:size(treeNodes, 1)
        # check different tree nodes
        if i != j
            # indicator that all elements are located in other tree node
            indAll = true
            for k in 1:size(treeNodes[i].graphNodes, 1)
                # check whether element is located in other tree node
                if !checkElementInNode(treeNodes, j, treeNodes[i].graphNodes[k])
                    # change boolean indicator
                    indAll = false
                    # exit loop
                    break
                end
            end
            # analyze results of comparison
            if indAll == true
                # change boolean indicator
                indSubset = true
                # exit loop
                break
            end
        end
    end
    # return subset indicator
    return indSubset

end

# function to check common element condition for two nodes of tree decomposition
function checkCommonElementCondition(treeNodes, i, j)

    # initialization
    indCommon = false
    # go through elements of the first tree node
    for k in 1:size(treeNodes[i].graphNodes, 1)
        # receive element of the first tree node
        elem = treeNodes[i].graphNodes[k]
        # go through all elements of the second tree node
        for t in 1:size(treeNodes[j].graphNodes, 1)
            # compare two elements
            if elem == treeNodes[j].graphNodes[t]
                # change boolean indicator
                indCommon = true
                # exit loop
                break
            end
        end
        # compare boolean indicator
        if indCommon == true
            # exit loop
            break
        end
    end
    # return subset indicator
    return indCommon

end

# function to check tree node in array
function checkNodeInArray(treeNodes, node)

    # make search
    indElem = false
    index = noNumber
    for i in 1:size(treeNodes, 1)
        if treeNodes[i] == node
            # change value of boolean indicator and index of element
            indElem = true
            index = i
            break
        end
    end
    # return boolean indicator and index of element
    return index, indElem

end

# function to check connectivity condition for specific graph node in tree decomposition
function checkConnectivityCondition(decomposition, addedNodes, idNode, listLinksTree, graphNode)

    # find all tree nodes with the given node
    treeNodes = Array{Int, 1}(undef, 0)
    for i in 1:size(addedNodes, 1)
        indexNode = addedNodes[i]
        for j in 1:size(decomposition.treeNodes[indexNode].graphNodes, 1)
            # add new tree node if it is required
            if decomposition.treeNodes[indexNode].graphNodes[j] == graphNode
                append!(treeNodes, indexNode)
                break
            end
        end
    end
    # add default tree node
    append!(treeNodes, idNode)

    # launch the first-breadth search for connectivity problem
    queue = Array{Int, 1}(undef, 1)
    queue[1] = treeNodes[1]
    deleteat!(treeNodes, 1)
    while size(queue, 1) != 0
        # take the first node in queue
        node = queue[1]
        deleteat!(queue, 1)
        # go through all neighbour tree nodes
        numConn = size(listLinksTree[node].connections, 1)
        for i in 1:numConn
            neighNode = listLinksTree[node].connections[i]
            # check whether this node is not visited
            indexNode, indElem = checkNodeInArray(treeNodes, neighNode)
            # check whether this node is not visited
            if indElem == true
                # add neighbour node in queue if it is possible
                append!(queue, neighNode)
                deleteat!(treeNodes, indexNode)
            end
        end
    end

    # determine connectivity indicator
    indConn = false
    if size(treeNodes, 1) == 0
        indConn = true
    end

    # return connectivity indicator
    return indConn

end
