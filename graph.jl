include("alltypes.jl")

# function to read graph from terminal
function readGraphFromFile(filename)

    # open file reader
    fileReader = open(filename, "r")
    # read number of nodes in graph
    row = readline(fileReader)
    numNodes = parse(Int, row)
    # create graph structure
    graph = Graph(numNodes)

    # go through all nodes
    for i in 1:numNodes

        # read new row
        row = readline(fileReader)
        # split row into several substrings
        substrings = split(row, " ")
        # find out number of connections (substrings)
        numConn = size(substrings, 1)
        # create link and array of connections for the given link
        graph.listLinks[i] = Link(numConn)

        # go through all substrings
        for j in 1:numConn
            # convert substrings to numbers
            numLink = parse(Int, substrings[j])
            # save number as connection in special array
            graph.listLinks[i].connections[j] = numLink
        end

    end

    # go throug all array of connections
    for i in 1:numNodes
        # sort array of connections
        sort!(graph.listLinks[i].connections)
    end

    # close file reader
    close(fileReader)
    # return graph
    return graph

end

# function to delete node in graph
function deleteNodeInGraph(graph, idNode)

    # determine number of element in array of linked nodes
    numLinks = size(graph.listLinks[idNode].connections, 1) + 1
    # save linked nodes together with deleted node
    linkedNodes = Array{Int, 1}(undef, numLinks)
    linkedNodes[numLinks] = idNode

    # go through all linked nodes
    for i in 1:numLinks - 1
        # save deleted links in special array
        linkedNodes[i] = graph.listLinks[idNode].connections[i]
    end

    # delete all linked nodes (rows)
    graph.listLinks[idNode].connections = Array{Int, 1}(undef, 0)
    # go through all nodes of graph
    for i in 1:graph.numNodes
        # find number of connection
        numConn = size(graph.listLinks[i].connections, 1)
        # go through all connections
        for j in 1:numConn
            # delete all linked nodes (columns) if it is required
            if graph.listLinks[i].connections[j] == idNode
                deleteat!(graph.listLinks[i].connections, j)
                # exit loop
                break
            end
        end
    end

    # return array of linked nodes
    return linkedNodes

end
