# JoSLoS

## Introduction

**TaSeTreeD** (Tabu Search Tree Decomposition) is software to find optimal
tree decomposition using tabu search algorithm \[0,1\].

## Usage

To run this software, you must enter the following command inside the project 
directory:

```bash
julia tesetreed.jl -i <inputFile> -o <outputFile> -n <numberSteps>.
```

Here, **\<inputFile\>** (default value is "graph.txt") and **\<outputFile\>** (default value is "tree.txt") are
names of input and output files, respectively. **\<numberSteps\>** (default value is 10000) should contain
the number of steps that will be performed to optimize tree decomposition.

The input file should be formed as follows: 

```go
<numNodes>
<node_1> <node_2> ... <node_n>
<node_1> <node_2> ... <node_m>
...
<node_1> <node_2> ... <node_l>
```

The first row should contain field **\<numNodes\>**, which specifies number of graph nodes for
tree decomposition. After that, **\<numNodes\>** lines are needed to describe each node.
Each line of the description must contain identifiers of the nodes with which this node is
connected (adjacency list). Note that vertex identifiers start at one, not zero!

The output file will contain the execution time of all jobs according to the found optimal solution 
and table of special form to to specify the sequence of jobs for performing:

```go
Number of nodes: <numTreeNodes>
Elimination order:
<node> <node> .. <node>
Node 1: <node> <node> ... <node>
Node 2: <node> <node> ... <node>
....
Node <numTreeNodes>: <node> <node> ... <node>
Links
Link 1: <treeNode> <treeNode> ... <treeNode>
Link 2: <treeNode> <treeNode> ... <treeNode>
...
Link <numTreeNodes>: <treeNode> <treeNode> ... <treeNode>
```

**\<numTreeNodes\>** is number of tree nodes in found tree decomposition. **\<node\>** is
node identifiers of the initial graph, which are node labels of the found tree. **\<treeNode\>**
is the identifier of the tree node, which is created after optimization of the tree decomposition.
The output file contains list of ID graph nodes for each tree decomposition node, adjacency list
for specifying tree connections as well as the elimination order in which the optimal tree
decomposition was obtained.

Examples of input and output files for the given software are located in folder **examples**.

## Dependency

For execution of the given software, you need to install the **Julia** programming language interpreter, 
as well as the **ArgParse** library for parsing parameters that are transmitted through the terminal. 

## License

This software has MIT License. The text of this license is located below. 

Copyright (c) 2022 Artem Tomilo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Reference

[0] Yai-Fung Lim, Pei-Yee Hong, R. Ramli and R. Khalid, "An improved tabu search
for solving symmetric traveling salesman problems," 2011 IEEE Colloquium on Humanities,
Science and Engineering, 2011, pp. 851-854, doi: 10.1109/CHUSER.2011.6163857.

[1] H. L. Bodlaender and A. M. C. A. Koster, "Combinatorial Optimization on Graphs
of Bounded Treewidth," in The Computer Journal, vol. 51, no. 3, pp. 255-269, May 2008,
doi: 10.1093/comjnl/bxm037.
