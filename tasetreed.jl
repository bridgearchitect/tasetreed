using ArgParse

include("terminal.jl")
include("alltypes.jl")
include("tree.jl")
include("graph.jl")
include("search.jl")

# parse options from terminal
filenameInput, filenameOutput, numIter = parseTerminal()
# read graph from file
graph = readGraphFromFile(filenameInput)

# create elimination order
elimOrder = createEliminationOrder(graph.numNodes)
# find the best tree decomposition
decomposition = searchTreeDecomposition(graph, elimOrder, numIter)
# function to make tree decomposition
makeTreeLinks(decomposition)

# write tree decomposition in file
writeTreeDecompositionInFile(decomposition, filenameOutput)
