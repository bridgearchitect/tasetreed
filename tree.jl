include("alltypes.jl")
include("graph.jl")
include("check.jl")

# function to make elimination order
function createEliminationOrder(numNodes)

    # create array of nodes
    nodes = Array{Int, 1}(undef, numNodes)
    # fill array of nodes
    for i in 1:numNodes
        nodes[i] = i
    end

    # create array of elimination order
    elimOrder = Array{Int, 1}(undef, numNodes)
    # go through all elements of elimination order
    for i in 1:numNodes
        # choose random element from array of nodes
        index = abs(rand(Int) % size(nodes, 1)) + 1
        # save chosen element in elimination order
        elimOrder[i] = nodes[index]
        # delete chosen element in array of nodes
        deleteat!(nodes, index)
    end

    # return array of elimination order
    return elimOrder

end

# function to make tree decomposition using specified elimination order
function makeTreeDecomposition(graph, elimOrder)

    # create object of decomposition
    decomposition = TreeDecomposition(elimOrder)
    # copy graph into another object
    curGraph = deepcopy(graph)

    # go through all elements of elimination order
    for i in 1:size(elimOrder, 1)
        # receive node for deleting
        delNode = elimOrder[i]
        # delete element in graph and save linked nodes
        linkedNodes = deleteNodeInGraph(curGraph, delNode)
        # sort array of linked nodes
        sort!(linkedNodes)
        # create new tree node for decomposition
        treeNode = TreeNode(linkedNodes)
        # add new tree node in array of tree decomposition nodes
        append!(decomposition.treeNodes, [treeNode])
        append!(decomposition.mainGraphNodes, [delNode])
    end

    # calculate tree width of decomposition
    maxValue = noNumber
    for i in 1:size(decomposition.treeNodes, 1)
        if size(decomposition.treeNodes[i].graphNodes, 1) > maxValue
            maxValue = size(decomposition.treeNodes[i].graphNodes, 1)
        end
    end
    # save found tree width in field of decomposition
    decomposition.treeWidth = maxValue - 1

    # return object of decomposition
    return decomposition

end

# fucntion to make tree links for found decomposition
function makeTreeLinks(decomposition)

    # find number of nodes
    numNodes = size(decomposition.elimOrder, 1)
    # find number of tree nodes
    numTreeNodes = size(decomposition.treeNodes, 1)
    # create array of connected arrays
    connectedNodes = Array{Int, 1}(undef, 0)
    # create and fill array to save links for tree decomposition
    listLinksTree = Array{Link, 1}(undef, size(decomposition.treeNodes, 1))
    for i in 1:size(decomposition.treeNodes, 1)
        listLinksTree[i] = Link()
    end

    # go through all tree nodes
    for i in 2:size(decomposition.treeNodes, 1)
        # receive main graph node of specific tree node
        mainGraphNode = decomposition.mainGraphNodes[i]
        j = i - 1
        while j >= 1
            # check condition for connectivity
            if checkElementInNode(decomposition.treeNodes, j, mainGraphNode) && (!checkElementInArray(connectedNodes, j))
                # add new node to connectivity array
                append!(connectedNodes, j)
                # add new link in tree decomposition
                append!(listLinksTree[i].connections, j)
                append!(listLinksTree[j].connections, i)
            end
            # go to the next iteration
            j -= 1
        end
    end

    # create and fill connection matrix
    decomposition.connectionMatrix = Array{Int, 2}(undef, numTreeNodes, numTreeNodes)
    for i in 1:numTreeNodes
        for j in 1:numTreeNodes
            decomposition.connectionMatrix[i,j] = noEdge
        end
    end
    for i in 1:numTreeNodes
        for j in 1:size(listLinksTree[i].connections, 1)
            decomposition.connectionMatrix[i,listLinksTree[i].connections[j]] = isEdge
        end
    end

    # go through all nodes of tree decomposition
    i = 1
    while i <= size(decomposition.treeNodes, 1)
        # check subset condition
        if checkSubsetCondition(decomposition.treeNodes, i)
            # create array of links for the found node
            listLink = Link()
            for j in 1:numTreeNodes
                if decomposition.connectionMatrix[i,j] == isEdge
                    append!(listLink.connections, j)
                end
            end
            # delete extra tree node
            deleteat!(decomposition.treeNodes, i)
            deleteat!(decomposition.mainGraphNodes, i)
            # delete extra connection in matrix
            decomposition.connectionMatrix = decomposition.connectionMatrix[1:end .!= i, 1:end .!= i]
            # modify array of links
            for j in 1:size(listLink.connections, 1)
                if listLink.connections[j] > i
                    listLink.connections[j] -= 1
                end
            end
            # make additional connections if it is possible
            if size(listLink.connections, 1) != 0
                idMainNode = listLink.connections[1]
                for j in 2:size(listLink.connections, 1)
                    idSubNode = listLink.connections[j]
                    decomposition.connectionMatrix[idMainNode, idSubNode] = isEdge
                    decomposition.connectionMatrix[idSubNode, idMainNode] = isEdge
                end
            end
            # recalculate number of tree nodes
            numTreeNodes = size(decomposition.treeNodes, 1)
            # decrease iteration variable by one
            i -= 1
        end
        # go to the next iteration
        i += 1
    end

end

# function to write tree decomposition in file
function writeTreeDecompositionInFile(decomposition, filename)

    # open file writer
    fileWriter = open(filename, "w")

    # write general information about tree nodes
    numTreeNodes = size(decomposition.treeNodes, 1)
    write(fileWriter, "Number of nodes: " * string(numTreeNodes) * "\n")

    # write elimination order
    write(fileWriter, "Elimination order:\n")
    for i in 1:size(decomposition.elimOrder, 1)
        write(fileWriter, string(decomposition.elimOrder[i]) * " ")
    end
    write(fileWriter, "\n")

    # write information about every tree nodes
    for i in 1:numTreeNodes
        numGraphNodes = size(decomposition.treeNodes[i].graphNodes, 1)
        write(fileWriter, "Node " * string(i) * ": ")
        # go through all graph nodes
        for j in 1:numGraphNodes
            write(fileWriter, string(decomposition.treeNodes[i].graphNodes[j]) * " ")
        end
        write(fileWriter, "\n")
    end

    # write information about links of every node
    write(fileWriter, "Links\n")
    for i in 1:numTreeNodes
        # write intial symbols
        write(fileWriter, "Link " * string(i) * ": ")
        # create array of links for the found node
        listLink = Link()
        for j in 1:numTreeNodes
            if decomposition.connectionMatrix[i,j] == isEdge
                append!(listLink.connections, j)
            end
        end
        # find number of links
        numLinks = size(listLink.connections, 1)
        # go through all links
        if numLinks != 0
            for j in 1:numLinks
                write(fileWriter, string(listLink.connections[j]) * " ")
            end
            write(fileWriter, "\n")
        else
            write(fileWriter, "empty\n")
        end
    end

    # close file writer
    close(fileWriter)

end
