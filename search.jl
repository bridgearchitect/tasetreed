include("alltypes.jl")
include("tree.jl")

# function to make all possible swaps
function makeAllSwaps(elimOrder)

    # find out number of nodes in graph
    numNodes = size(elimOrder, 1)
    # create array of swap structures
    swaps = Array{SwapOrder, 1}(undef, Int(numNodes * (numNodes - 1) / 2))

    # generate all swaps
    i = 1
    numSwap = 1
    while i <= numNodes
        j = i + 1
        while j <= numNodes
            # make swap
            newElimOrder = deepcopy(elimOrder)
            inter = newElimOrder[i]
            newElimOrder[i] = newElimOrder[j]
            newElimOrder[j] = inter
            # save swap in array
            swap = SwapOrder(i, j, newElimOrder)
            swaps[numSwap] = swap
            # go to the next iteration
            numSwap += 1
            j += 1
        end
        # go to the next iteration
        i += 1
    end

    # return array of swaps
    return swaps

end

# function to generate decompositions for all swapd elimination orders
function generateDecompositions(graph, swaps)

    # find out number of swaps
    numSwap = size(swaps, 1)
    # create array of new decompositions
    arrayDecom = Array{TreeDecompositionSwap, 1}(undef, numSwap)

    # go through all swaps
    for k in 1:numSwap
        # create new decomposition
        newDecomposition = makeTreeDecomposition(graph, swaps[k].elimOrder)
        # save new decomposition in array
        i = swaps[k].i
        j = swaps[k].j
        arrayDecom[k] = TreeDecompositionSwap(i, j, newDecomposition)
    end

    # return array of decompositions
    return arrayDecom

end

# function to choose the best decomposition in neighbourhood
function findBestDecompositionNeigh(arrayDecom)

    # find out number of tree decompositions
    numDecom = size(arrayDecom, 1)
    # search the best decomposition
    minValue = inf
    indexBest = noNumber
    for i in 1:numDecom
        if arrayDecom[i].decom.treeWidth < minValue
            minValue = arrayDecom[i].decom.treeWidth
            indexBest = i
        end
    end
    # return index of the best decomposition
    return indexBest

end

# function to find all non-tabu swaps
function findAllNonTabuSwaps(tabuList, numNodes)

    # create array of non-tabu swaps
    nonTabuSwaps = Array{Swap, 1}(undef, 0)
    # go through all swaps
    i = 1
    while i <= numNodes
        j = i + 1
        while j <= numNodes
            # check tabu conditions
            if tabuList[i,j] == 0
                append!(nonTabuSwaps, [Swap(i,j)])
            end
            # go to the next iteration
            j += 1
        end
        # go to the next iteration
        i += 1
    end
    # return array of swaps
    return nonTabuSwaps

end

# function to find the oldest swap
function findOldestSwap(lastMemory, numNodes)

    # go through all swaps
    i = 1
    maxValue = noNumber
    iMax = noNumber
    jMax = noNumber
    while i <= numNodes
        j = i + 1
        while j <= numNodes
            # find the oldest swap
            if lastMemory[i,j] > maxValue
                maxValue = lastMemory[i,j]
                iMax = i
                jMax = j
            end
            # go to the next iteration
            j += 1
        end
        # go to the next iteration
        i += 1
    end

    # create and return swap object
    swap = Swap(iMax, jMax)
    return swap

end

# function to update tables for tabu algorithm
function updateTables(tabuList, lastMemory, numNodes)

    # go through all swaps
    i = 1
    while i <= numNodes
        j = i + 1
        while j <= numNodes
            # update values of tables
            if tabuList[i,j] != 0
                tabuList[i,j] -= 1
            end
            lastMemory[i,j] += 1
            # go to the next iteration
            j += 1
        end
        # go to the next iteration
        i += 1
    end

end

# function to apply swap to elimination order
function applySwapToEliminationOrder(elimOrder, swap)

    # create new elimination order
    newOrder = deepcopy(elimOrder)
    # receive indeces of swap
    i = swap.i
    j = swap.j
    # make swap
    inter = newOrder[i]
    newOrder[i] = newOrder[j]
    newOrder[j] = inter
    # return new elimination order
    return newOrder

end

# function to launch search of the best tree decomposition using tabu algorithm
function searchTreeDecomposition(graph, elimOrder, numIter)

    # find out number of nodes in graph
    numNodes = graph.numNodes
    # generate initial decomposition
    bestDecomposition = makeTreeDecomposition(graph, elimOrder)
    curDecomposition = deepcopy(bestDecomposition)
    # save initial the best tree width
    bestWidth = bestDecomposition.treeWidth
    curWidth = bestWidth
    # create tabu list and the last usage memory
    tabuList = zeros(Int, (numNodes, numNodes))
    lastMemory = zeros(Int, (numNodes, numNodes))

    # launch tabu search
    for k in 1:numIter
        # make all possible swaps
        swaps = makeAllSwaps(elimOrder)
        # generate all decompositions for the given swaps
        arrayDecom = generateDecompositions(graph, swaps)
        # find index of the best decomposition
        indexBest = findBestDecompositionNeigh(arrayDecom)
        # check whether it is better than current decomposition
        if arrayDecom[indexBest].decom.treeWidth < curWidth
            # change current solution
            curWidth = arrayDecom[indexBest].decom.treeWidth
            curDecomposition = arrayDecom[indexBest].decom
            # save new swap in tables
            i = arrayDecom[indexBest].i
            j = arrayDecom[indexBest].j
            tabuList[i,j] = tabuIter
            lastMemory[i,j] = 0
        else
            # find all non-tabu swaps
            nonTabuSwaps = findAllNonTabuSwaps(tabuList, numNodes)
            if size(nonTabuSwaps, 1) != 0
                # choose random swap
                randNumber = abs(rand(Int)) % size(nonTabuSwaps, 1) + 1
                # save random swap as current decomposition
                newOrder = applySwapToEliminationOrder(curDecomposition.elimOrder, nonTabuSwaps[randNumber])
                curDecomposition = makeTreeDecomposition(graph, newOrder)
                curWidth = curDecomposition.treeWidth
            else
                # find the oldest swap
                swap = findOldestSwap(lastMemory, numNodes)
                # save the found random swap as current decomposition
                newOrder = applySwapToEliminationOrder(curDecomposition.elimOrder, swap)
                curDecomposition = makeTreeDecomposition(graph, newOrder)
                curWidth = curDecomposition.treeWidth
            end
        end
        # update tables
        updateTables(tabuList, lastMemory, numNodes)
        # update the best decomposition
        if curWidth < bestWidth
            bestWidth = curWidth
            bestDecomposition = deepcopy(curDecomposition)
        end
    end

    # return the best decomposition
    return bestDecomposition

end
