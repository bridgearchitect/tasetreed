# define structures

# define structure to describe swap
mutable struct Swap
    i::Int
    j::Int
end
# defualt constructor
Swap() = Swap(noNumber, noNumber)

# define structure to describe swap in elimination order
mutable struct SwapOrder
    i::Int
    j::Int
    elimOrder::Array{Int, 1}
end
# defualt constructor
SwapOrder() = SwapOrder(noNumber, noNumber, Array{Int, 1}(undef, 0))

# define structure to save tree nodes
mutable struct TreeNode
    graphNodes::Array{Int, 1}
end
# default constructor
TreeNode() = TreeNode(Array{Int, 1}(undef, 0))
# modified constructor
TreeNode(graphNodes) = TreeNode(graphNodes)

# define structure to save links between nodes
mutable struct Link
    connections::Array{Int, 1}
end
# default constructor
Link() = Link(Array{Int, 1}(undef, 0))
# modified constructor
Link(numConn) = Link(Array{Int, 1}(undef, numConn))

# define structure to save tree decomposition of graph
mutable struct TreeDecomposition
    elimOrder::Array{Int, 1}
    treeWidth::Int
    treeNodes::Array{TreeNode, 1}
    mainGraphNodes::Array{Int, 1}
    connectionMatrix::Array{Int, 2}
end
# default constructor
TreeDecomposition() = TreeDecomposition(Array{Int, 1}(undef, 0), noNumber, Array{TreeNode, 1}(undef, 0),
                                        Array{Int, 1}(undef, 0), Array{Int, 2}(undef, 0, 0))
# modified constructor
TreeDecomposition(elimOrder) = TreeDecomposition(elimOrder, noNumber, Array{TreeNode, 1}(undef, 0),
                                                 Array{Int, 1}(undef, 0), Array{Int, 2}(undef, 0, 0))

# define structure to save tree decomposition with swaped indexes
mutable struct TreeDecompositionSwap
    i::Int
    j::Int
    decom::TreeDecomposition
end

# define structure to save usual graph
mutable struct Graph
    numNodes::Int
    listLinks::Array{Link, 1}
end
# default constructor
Graph() = Graph(noNumber, Array{Link, 1}(undef, 0))
# modified constructor
Graph(numNodes) = Graph(numNodes, Array{Link, 1}(undef, numNodes))

# define constants

# string constants
defaultInputFilename = "graph.txt"
defaultOutputFilename = "tree.txt"
noString = ""

# number constants
noNumber = -1
inf = 1000000000
defaultNumIter = 10000
tabuIter = 10
startNode = 1
visited = 1
nonVisited = 0
isEdge = 1
noEdge = 0
