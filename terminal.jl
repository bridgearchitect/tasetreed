include("alltypes.jl")

# function to define rules of parsing for terminal
function defineRulesParseTerminal()

    # define rules
    rules = ArgParseSettings()
    @add_arg_table! rules begin
        "-i"
            help = "name of input file"
            default = defaultInputFilename
        "-o"
            help = "name of output file"
            default = defaultOutputFilename
        "-n"
            help = "number of iterations"
            default = defaultNumIter
            arg_type = Int
    end
    # return rules
    return parse_args(rules)

end

# function to parse options from terminal
function parseTerminal()

    # initialzation
    filenameInput = noString
    filenameOutput = noString
    numIter = noNumber

    # parse data from terminal
    parsedArgs = defineRulesParseTerminal()
    # save parsed data
    for (arg,val) in parsedArgs
        if arg == "i"
            filenameInput = val
        elseif arg == "o"
            filenameOutput = val
        elseif arg == "n"
            numIter = val
        end
    end

    # return parsed data
    return filenameInput, filenameOutput, numIter

end
